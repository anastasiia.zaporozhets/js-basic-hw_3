"use strict"

let age = +prompt('Введіть ваш вік');

if (Number.isNaN(age)) {
    alert("Ви ввели не число!")
} else if (age < 12) {
    alert("Ви ще дитина!")
} else if (age < 18) {
    alert("Ви ще підліток!")
} else {
    alert("Ви є дорослим!")
}



let month = prompt('Введіть назву місяця');

switch (month.toLowerCase()) {
    case 'січень':
        alert('В січні 31 день');
        break;
    case 'лютий':
        alert('В лютому 29 днів');
        break;
    case 'березень':
        alert('В березні 31 день');
        break;
    case 'квітень':
        alert('В квітні 30 днів');
        break;
    case 'травень':
        alert('В травні 31 день');
        break;
    case 'червень':
        alert('В червні 30 днів');
        break;
    case 'липень':
        alert('В липні 31 день');
        break;
    case 'серпень':
        alert('В серпні 31 день');
        break;
    case 'вересень':
        alert('В вересні 30 днів');
        break;
    case 'жовтень':
        alert('В жовтні 31 день');
        break;
    case 'листопад':
        alert('В листопаді 30 днів');
        break;
    case 'грудень':
        alert('В грудні 31 день');
        break;
    default:
        alert('невірна назва місяця')
}



